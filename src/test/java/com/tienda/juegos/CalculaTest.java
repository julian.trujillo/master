package com.tienda.juegos;
import  com.tienda.juegos.negocio.Calculadora;


import static org.junit.Assert.*;

import org.junit.Test;


public class CalculaTest {

	@Test
	public void addtest() {
		//Arrange
		//A
		//Assertions
		int a=1;
		int b=1;
		int resultado;
		Calculadora cal= new Calculadora();
		resultado=cal.add(a,b);
		assertEquals(2,resultado);
		
		
	}
	
	@Test
	public void subtest() {
		//Arrange
		//A
		//Assertions
		int a=1;
		int b=1;
		int resultado;
		Calculadora cal= new Calculadora();
		resultado=cal.sub(a,b);
		assertEquals(0,resultado);
		
		
	}

}
